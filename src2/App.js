import logo from "./logo.svg";
import "./App.css";
import "./App.less";
import { Button, Pagination } from "antd";

function App() {
  const itemRender = (_, type, originalElement) => {
    if (type === "prev") {
      return <a href="/asd">Previous</a>;
    }

    if (type === "next") {
      return <a href="/asd">Next</a>;
    }

    return originalElement;
  };
  return (
    <div className="App">
      <Button type="primary">Primary Button</Button>
      <Button>Default Button</Button>
      <Button type="dashed">Dashed Button</Button>
      <br />
      <Button type="text">Text Button</Button>
      <Button type="link">Link Button</Button>
      <Pagination total={500} itemRender={itemRender} />
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
